# JukeBox

A web service to create playlists and play music from an existing local collection of music files.

I did this because every time I upgrade my Android phone I loose my playlists and the phone has no ability to export playlists so they can be edited programmatically.

The goal is to use JukeBox to listen to music at my desk, add tracks to playlist and then download the playlists with the tracks directly to my phone.

Currently only the playlist management works. Media player and queue buttons do nothing.

![Screenshot](backend/docs/Screenshot_01.JPG)

## Install
Checkout the code from GitLab.com
```
git clone https://gitlab.com/SiliconTao-open-source/JukeBox.git
```

Install the Perl dependencies for [MusicScan.pl](./Tools/MusicScan.pl)
```
sudo apt install libmp3-tag-perl
```

Install the NodeJS dependencies
```
cd JukeBox/backend
node install
```

No other packages formats, such as Docker, are provided at this time. If you would like to help create other package requirements, please fork the repository and send me a pull request.


## Useage
Change into the JukeBox/backend directory.

Running the service if you want to change the code
```
nodemon api/service.js
```

Running the service if you just want to use it
```
pm2 start api/service.js
```

Open your web browser to the server hosting JukeBox at port 6001.
Example
```
http://192.168.0.2:6001
```

This will build the raw empty DB.

Use Ctrl-C to exit Node and stop the service. Change to the JukeBox directory. Populate the DB with information about your existing music collection. If you have a large collection this will take some time. Do not use the JukeBox web page while MusicScan is doing this first initial build of the DB, MusicScan has exclusive lock on the DB.
Example
```bash
./Tools/MusicScan.pl ./backend/JukeBox.db /home/steve/My\ Music/
```

Once MusicScan.pl is done, reload your web browser with the JukeBox page.

Default view loads all tracks into the table.
Create a new playlist using the menu.
Click on a track in the table to select it.
Click on a playlist in the menu, click 'Add Selected' to add that track to the playlist
Click on a playlist in the menu, click 'Load' to load the playlist tracks into the table.
Click on a playlist in the menu, click 'Download' to download a zip file of the playlist.
With a playlist loaded, selecting a track in the table will allow for the track to be removed from the playlist.


## [MusicScan.pl](./Tools/MusicScan.pl)
This tool is used to populate the SQLite3 DB with data about the music files on you server.

It accepts two arguments:
1. The path and filename of the SQLite3 DB file.
2. The MusicRoot directory to scan for MP3 files.

### Known Issues
#### DB Locking
When the SQLite DB is under heavy use it can run into issues where the commands are blocking each other and an error like this can happen.
```
DBD::SQLite::st execute failed: database disk image is malformed at ./Tools/MusicScan.pl line 56.
```
Make sure to stop the NodeJS service while building the initial DB. This will reduce the number of programs accessing the DB file.

#### Invalid ID3 Tag
This error could mean that the ID3 tag is not formatted correctly or contains characters that need to be escaped or are UTF-8.
```
Use of uninitialized value in subtraction (-) at /usr/share/perl5/MP3/Tag/ID3v2.pm line 2070.
```

There is no known work-around at this time.



## [Backend](./backend/api/service.js)
This is the ExpressJS back-end service. It was designed to be the API for JukeBox but it also includes very simple interface using jQuery resources.

Currently the back-end playlist features are working. The media player is coming next.

This service will be exposed to all Ethernet devices so it can be used from any device on the LAN.

This has not been hardened for public facing Internet access. There is currently no login required and values are not sanitized. Use at your own risk.

Using the WSI of the backend will load jQuery libraries from [DataTables.net](https://datatables.net) and [jQuery.com](https://jquery.com)

The back-end can now serve album art if there are image files named **cover** or **folder** in the folder. The code to read the image from the ID3 tag when there is no image file, does not work. [node-id3 issue 54](https://github.com/Zazama/node-id3/issues/54)

## Frontend
This has not been worked on yet. Currently the back-end provides a simple WSI.

Originally I was planning to make the front-end in AnjularJS but have recently started to look at React. I will do some testing and make a decision later.
