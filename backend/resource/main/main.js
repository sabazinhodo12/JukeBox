function Main() {

var Dialog = 0;
var SelectedTrackId = 0;

let SelectedView = "All Tracks";
let PlayQueue = [];
let PlayHistory = [];
let PlayerState = 0;
let CurrentTrackTitle;
let table;
let TableInitialized = 0;

this.Initilaize = function() {
	$( "#menu" ).menu();
 
	// This is the playlist menu
	this.ResetPlaylistMenu();

	// This is to setup the data table view of the tracks
	// This would be a cool feature. https://datatables.net/examples/api/row_details.html
	$(document).ready(function() {
		table = $('#TracksTable').DataTable({
			"autoWidth": false,
			ajax: {
				url: "/TracksTable/1",
				dataSrc: ''
			},
			columns: [ 
				{ data: 'id' },
				{ data: 'artist' },
				{ data: 'title' },
				{ data: 'year' },
				{ data: 'album' },
				{ data: 'genre' }
			],
			select: true,
		});
		table.column(0).visible(false);
		$("#divtable").css({'width': '100%'});
		$("#TracksTable").css({'width': '100%'});
		//$('#TracksTable').DataTable().ajax.url("/TracksTable/0").load(function() {
			Main1.LoadNextPage("/TracksTable/");
		//} );
		
		//$("#divtable").css( 'display', 'block' );
		//table.columns.adjust().draw();
	});


	// Looking for DataTable events
	$('#TracksTable').on("click", 'tr', function(){
		// This only gets the track ID if the ID column is visible.
		// console.log(e);
		//let TrackSelectedFull = e.target.parentElement.innerText;
		//console.log(TrackSelectedFull);
		//Main1.ToggleSelectedTrackId(TrackSelectedFull.match(/\d+/m)[0]);		

		// console.log('e = ', e);
		//console.log( table.row( this ).data().id );
		Main1.ToggleSelectedTrackId(table.row( this ).data().id);
	});

	$("#PauseButton").click(function() {
		document.getElementById("mediaplayer").pause();
	});

	$("#NextButton").click(function() {		
		if(PlayQueue.length == 0) {
			// Play in order
			table.rows({search:'applied'}).iterator( 'row', function ( context, index ) {
				PlayQueue.push(this.row( index ).data().id);
			});			
		}		
		Main1.NextTrack();
	});

	$("#RandomButton").click(function() {
		Main1.SetRandomTracks();
	});

	// Add an event handler to the search box in the data table so that if search 
	// results are less then 100 it will query the server for a new data set.
	// Then paging on the server side will not be needed.

	$("#divtable").css("padding-left", "30px");
	$(".buttons").css( { "padding-left": "20px", "padding-right": "20px" });
	// $("#divtable").outerWidth("100%");

	// The Ajax POST. The offical documentation does not work. Had to remove the form.
	// https://www.w3schools.com/jquery/jquery_ajax_get_post.asp
	$("#newplaylistbutton").click( function(event) {
			$.post(
				'/newplaylist/',
				{
					newplaylistname: $('#newplaylistname').val()
				},				
				function (data, staus) {	
					$( "#PopupOverlay" ).dialog("close");				
					// alert(data);
					Main1.ShowMessage(data);
					Main1.ResetPlaylistMenu();
				}
			);
		}
	);
}

this.AddToPlayQueue = function(TrackId) {
	// console.log("AddToPlayQueue("+TrackId+")");
	PlayQueue.push(TrackId);
	//console.log("AddToPlayQueue PlayQueue.length = "+PlayQueue.length );

	// If not playing, start player.
	if(PlayerState == 0) {
		PlayerState = PlayQueue.shift();
    	Main1.PlayTrack(PlayerState);
	}
}

this.SetRandomTracks = function() {
	// console.log('SetRandomTracks');

	// This is ugly. There are three steps, each with their own loop.
	// I would like to do this with one loop by pulling from the DataTable in a random order.
	let Add = [];
	table.rows({search:'applied'}).iterator( 'row', function ( context, index ) {
		//console.log('adding track '+this.row( index ).data().id);
    	Add.push(this.row( index ).data().id);
	});
	for(let i = 0; i < Add.length; i++) {
		const j = Math.floor(Math.random() * (Add.length + 1));        
		//console.log("Track id "+j+" selected for slot "+i);
		if(PlayerState == 0) {
			Main1.AddToPlayQueue(j);
		} else {
			[Add[i], Add[j]] = [Add[j], Add[i]];			
		}
	}

	for(let i = 0; i < Add.length; i++) {
		let j = Add[i];
		if((!PlayQueue.includes(j)) && (! PlayHistory.includes(j))) {
			PlayQueue.push(j);
		}
	}
	/*if(PlayerState == 0) {
		Main1.NextTrack();
	}*/
}

this.NextTrack = function() {
	PlayerState = 0;
	if(PlayQueue.length > 0) {
		PlayerState = PlayQueue.shift();
		Main1.PlayTrack(PlayerState);
	}
}

this.PlayTrack = function(TrackId) {
	console.log("PlayTrack("+TrackId+")");

	// $('#mediaplayer').attr("src='/play/"+PlayerState+"'");
	// $('#mediaplayer').html("<source src='play/"+PlayerState+"' type='audio/mpeg'>");
	// document.getElementById("mediaplayer").play();
	//document.getElementById("mediasource").setAttribute('src', 'play/'+PlayerState);
	//document.getElementById("mediaplayer").play();
	
	//  This works with the sample.
	// https://stackoverflow.com/questions/8489710/play-an-audio-file-using-jquery-when-a-button-is-clicked
	//document.getElementById("mediasource").setAttribute('src', 'samples/1.mp3');
	document.getElementById("mediasource").onerror = function() {
		//Main1.ShowMessage("Error playing track "+CurrentTrackTitle);
		$.post(
			'/errorplayingtrack/',
			{
				trackid: PlayerState
			},				
			function (data, staus) {	
				Main1.ShowMessage(data);
			}
		);		
		Main1.NextTrack();
	};
	document.getElementById("mediasource").setAttribute('src', 'stream/'+PlayerState);
	document.getElementById("mediaplayer").onended = () => {
		PlayerState = 0;

		if(PlayQueue.length > 0) {
			PlayerState = PlayQueue.shift();
			Main1.PlayTrack(PlayerState);
		}
	};

	document.getElementById("mediaplayer").load();
	document.getElementById("mediaplayer").play();
	$.get('trackinfo/'+PlayerState,  function(response) {
		var Json = $.parseJSON(response);
		// Not wanting to do blank and !defined as two lines. 
		let TempArtis = Json[0].artist + '';
		let TempTitle = Json[0].title + '';
		let TempTrack = Json[0].track + '';
		if((TempArtis.length > 0) && (TempTitle.length > 0)) {
			CurrentTrackTitle = TempArtis+' - '+TempTitle;
		} else {
			CurrentTrackTitle = TempTrack;
		}		
		$("#current_track").html(CurrentTrackTitle);
	});

	PlayHistory.push(PlayerState);
	// console.log("PlayHistory.length = "+PlayHistory.length);
	document.getElementById("AlbumArt").setAttribute('src', 'albumart/'+PlayerState);

	// console.log('mediasource ='+document.getElementById("mediasource").getAttribute('src'));

	//var audioElement = document.createElement('audio');
	//audioElement.setAttribute('src', 'play/'+PlayerState);
	//audioElement.play();

	// Another way
	//function play_audio(task) {
    //  if(task == 'play'){
    //       $(".my_audio").trigger('play');
    //  }
    //  if(task == 'stop'){
    //       $(".my_audio").trigger('pause');
    //       $(".my_audio").prop("currentTime",0);
    //  }
 
}

this.ToggleSelectedTrackId = function(NewId) {
	if(SelectedTrackId === NewId) {
		// console.log('unset track '+NewId);
		SelectedTrackId = 0;		
	} else {
		// console.log('set track '+NewId);
		SelectedTrackId = NewId;
		PlayerState = NewId;
		//Main1.AddToPlayQueue(NewId);
		Main1.PlayTrack(NewId);
	}
	Main1.MenuAddRemoveSelector(SelectedTrackId);	
}

this.MenuAddRemoveSelector = function(TrackId) {
	// If track selected & in a playlist = add is off
	// No track selected & in a playlist = add is off
	// If track selected & in all tracks = add is on
	// No track selected & in all tracks = add is off
	// If track selected & in a playlist = remove is on
	// No track selected & in a playlist = remove is off
	// If track selected & in all tracks = remove is off
	// No track selected & in all tracks = remove is off		
	// So the default is off. If a track is selected, one menu item will be on, the other off.
	// This is a simple UI so it will not check with the DB if a track is currently in the playlist.

	SelectedTrackId = TrackId;
	$("#menu").find("li").each(function(i) {
		if((this.id.match(/as_\d/)) || (this.id.match(/rm_\d/)))	{
			$("#"+this.id).addClass('ui-state-disabled');
			if(SelectedTrackId > 0) {
				// If track selected & in all tracks = add is on
				if((SelectedView == "All Tracks") && (this.id.match(/as_\d/))) {
					$("#"+this.id).removeClass('ui-state-disabled');
				}

				// If track selected & in a playlist = remove is on
				if((SelectedView != "All Tracks") && (this.id.match(/rm_\d/))) {
					$("#"+this.id).removeClass('ui-state-disabled');
				}
			}
		}
	});
}


this.ResetPlaylistMenu = function() {
	$("#menu").html('');	
	$.get("/playlists", this.BuildPlaylistMenu);	
}

this.CreateNewPlaylist = function() {
	$("#PopupOverlayText").html('');
	if(Dialog > 0) { 
		// console.log("close the dialog");
		$( "#PopupOverlay" ).dialog("close"); 
	}
	Dialog = 1;
	$( "#PopupOverlay" ).dialog({width: 300, height:100, title:'New playlist' });
	$( "#PopupOverlay" ).css('visibility', 'visible');
}

this.ShowMessage = function(MessageText) {
	$( "#MessageBox" ).html(MessageText);
	$( "#MessageBox" ).dialog({width: 500, height:200, title:'New playlist' });
	$( "#MessageBox" ).css('visibility', 'visible');
	$( "#MessageBox" ).fadeOut({duration: 5000, complete: function() {
		$( "#MessageBox").dialog("close");
	}});
}

this.BuildPlaylistMenu = function(response) {
	var Json = $.parseJSON(response);
	var NewMenu = "<li id='ac_alltracks'><div>All Tracks</div></li>";
	NewMenu += "<li id='ac_newpl'><div>New playlist</div></li>";
	for (key in Json) {
			let PlId = Json[key].id;
			// PlId.replace(/ /g, "_");

			// Playlist menu entry
			NewMenu += "<li id='pl_"+PlId+"'>\n\t";
			NewMenu += "<div>"+Json[key].labelName+"</div><ul>";

			// Submenu to load or download the playlist
			NewMenu += "<li id='lo_"+PlId+"' name='"+Json[key].labelName+"''><div>Load</div></li>";
			NewMenu += "<li id='dl_"+PlId+"'><div>Download</div></li>";
			NewMenu += "<li id='ap_"+PlId+"'><div>Add Track</div></li>";
			NewMenu += "<li id='rm_"+PlId+"'><div>Remove Track</div></li>";

			NewMenu += "</ul></li>\n";
		}

		$("#menu").html(NewMenu);

		$("#menu").menu({
			select: function( event, ui ) {
				// console.log('menu selected id = '+ui.item.attr('id'));
				let IdSelected = ui.item.attr('id');

				// Action Control new playlist
				if(IdSelected.match('ac_newpl')) {
					Main1.CreateNewPlaylist();
				}

				// Action Control view all tracks
				if(IdSelected.match('ac_alltracks')) {					
					SelectedView = "All Tracks";
					$("#current_playlist").html(ui.item.text());
					$('#TracksTable').DataTable().search('').draw();
					$('#TracksTable').DataTable().ajax.url("/TracksTable/0").load(function() {
						Main1.LoadNextPage("/TracksTable/");
					} );
					SelectedTrackId = 0;
					Main1.MenuAddRemoveSelector(0);	
				}

				// LOad playlist
				if(IdSelected.match(/lo_*/)) {
					SelectedView = ui.item.attr('name');
					$("#current_playlist").html(SelectedView);					
					table.search('').draw();
					$('#TracksTable').DataTable().ajax.url("/playlist/"+IdSelected.replace(/lo_/, '')).load();
					SelectedTrackId = 0;
					Main1.MenuAddRemoveSelector(0);	
				}

				// DownLoad playlist
				if(IdSelected.match(/dl_*/)) {
					// console.log("/download/"+IdSelected.replace(/dl_/,""));
					//var link = document.createElement('a');
					//link.href = window.location("/download/"+IdSelected.replace(/dl_/,""));
    			//document.body.appendChild(link);
    			//link.click();
    			//document.body.removeChild(link);

					var xhr = new XMLHttpRequest();
					xhr.open("GET", "/download/"+IdSelected.replace(/dl_/,""));
					xhr.responseType = "arraybuffer";

					xhr.onload = function () {
						if (this.status === 200) {
							var blob = new Blob([xhr.response], {type: "application/zip"});
							var objectUrl = URL.createObjectURL(blob);
							window.open(objectUrl);
						}
					};
					xhr.send();

					// Created a branch to test archiver for building the zip file.

					//$.get("/download/"+IdSelected.replace(/dl_/,""), function(response) {
					

					// Simplified version of the exact same problem, file is twice as big and corrupt.
					//	var blob = new Blob([response], {type: "application/zip"});
        	//	var objectUrl = URL.createObjectURL(blob);
        	//	window.open(objectUrl);
        	//});

					// saveAs is not a function. 
						//var blob = new Blob([response], {type: "octet/stream"});
        		//var fileName = "playlist.zip";
        		//saveAs(blob, fileName);
        	//});

					// This downloads a file, looks to be almost double the correct size, not a valid zip. Errors out while trying to extract. 
					//	var blob = new Blob( [response], {type: "application/zip"});
					//	var link = document.createElement('a');
					//	link.href = window.URL.createObjectURL(blob);
					//	link.download = 'playlist.zip';
    			//	//window.location = link;
    			//	document.body.appendChild(link);
    			//	link.click();
    			//	document.body.removeChild(link);
					//});	

					// This does not show any errors but the download comes back very fast and only has 11 bytes.
					//var element = document.createElement('a');
  				//element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent("/download/"+IdSelected.replace(/dl_/,"")));
  				//element.setAttribute('download', 'playlist.zip');
  				//element.style.display = 'none';
  				//document.body.appendChild(element);
  				//element.click();
  				//document.body.removeChild(element);
				}


				// Add playing track to playlist
				if(IdSelected.match(/ap_*/)) {
					if(PlayerState > 0) {
						// console.log("Add selected track to playlist");
						$.post(
							'/addtoplaylist/',
							{
								playlistid: IdSelected.replace(/ap_/, ''),
								trackid: PlayerState
							},				
							function (data, staus) {	
								Main1.ShowMessage(data);
							}
						);
					} else {
						Main1.ShowMessage("No track is currently playing.");
					}
				}				

				// ReMove selected from playlist
				if(IdSelected.match(/rm_*/)) {
					//console.log("Remove selected track to playlist");
					$.post(
						'/removefromplaylist/',
						{
							playlistid: IdSelected.replace(/rm_/, ''),
							trackid: SelectedTrackId
						},				
						function (data, staus) {	
							Main1.ShowMessage(data);
							SelectedTrackId = 0;
							// console.log('reload the table');
							Main1.MenuAddRemoveSelector(0);	
							$('#TracksTable').DataTable().ajax.url("/playlist/"+IdSelected.replace(/rm_/, '')).load();
						}
					);
				}				
			}
		});

		$("#menu").menu("refresh");
		Main1.MenuAddRemoveSelector(SelectedTrackId);	
}

this.LoadNextPage = function(UriRquest) {
	// https://datatables.net/reference/api/ajax.url().load()
	let LastId = 0;
	// console.log("TableInitialized = "+TableInitialized);
	if(TableInitialized > 0) {
		LastId = table.row( ':last' ).data().id;
	}
	/***************************
	The DataTable Ajax call cannot append to the table so the Ajax call had to be done manually.
	https://datatables.net/forums/discussion/42806
	https://datatables.net/reference/api/rows.add()

	$('#TracksTable').DataTable().ajax.url(UriRquest+LastId).load(function() {
		Main1.LoadNextPage(UriRquest),
		false
	} );*/

	$.ajax({
		type: "GET",
		url: UriRquest+LastId,
		dataType: "json"
	})
		.done(function(response){
			if(response.length > 0) {
				table.rows.add(response);
				table.draw();
				Main1.LoadNextPage(UriRquest);
			}
		});
	TableInitialized = 1;	
}

};

let Main1 = new Main();
