BEGIN;

CREATE TABLE 'labels' (
	'id' INT NOT NULL, 
	'user' INT NULL, 
	'labelName' INT NULL, 
	'track' INT NULL, 
	PRIMARY KEY ('id'));
INSERT INTO 'labels' ('id') VALUES (0);


CREATE TABLE 'tracks' (
	'id' INT NOT NULL, 
	'path' INT NULL, 
	'track' VARCHAR(100) NULL, 
	'md5sum' CHAR(32) NULL, 
	'playTime' INT NULL, 
	'bitRate' INT NULL, 
	'fileSizeBytes' INT NULL, 
  'artist' VARCHAR(30) NULL, 
  'year' INT,
  'album' VARCHAR(60) NULL, 
  'title' VARCHAR(60) NULL, 
  'trackId' VARCHAR(60) NULL, 
  'genre' VARCHAR(30) NULL,
	PRIMARY KEY ('id'));
INSERT INTO 'tracks' ('id') VALUES (0);


CREATE TABLE 'paths' (
  'id' INT NOT NULL,
  'path' VARCHAR(250) NULL,
  PRIMARY KEY ('id'));
INSERT INTO 'paths' ('id') VALUES (0);


CREATE TABLE 'labelNames' (
  'id' INT NOT NULL,
  'labelName' VARCHAR(45) NULL,
  PRIMARY KEY ('id'));
INSERT INTO 'labelNames' ('id') VALUES (0);


CREATE TABLE 'languages' (
  'id' INT NOT NULL,
  'language' VARCHAR(45) NULL,
  'flag' VARCHAR(45) NULL,
  PRIMARY KEY ('id'));
INSERT INTO 'languages' ('id') VALUES (0);


CREATE TABLE 'users' (
  'id' INT NOT NULL,
  'login' VARCHAR(45) NULL,
  'passhash' VARCHAR(45) NULL,
  'language' INT NULL,
  PRIMARY KEY ('id'));
INSERT INTO 'users' ('id') VALUES (0);


CREATE TABLE 'config' (
  'parameter' VARCHAR(45) NULL,
  'value' VARCHAR(45) NULL,
  PRIMARY KEY ('parameter'));
INSERT INTO 'config' ('parameter','value') VALUES ('db_version', '1');


CREATE TABLE 'columnNames' (
  'id' INT NOT NULL,
  'language' INT NULL,
  'tableViewDOM' VARCHAR(45) NULL,
  'columnName' VARCHAR(45) NULL,
  'columnDOM' VARCHAR(45) NULL,
  PRIMARY KEY ('id'));
INSERT INTO 'columnNames' ('id') VALUES (0);

COMMIT;
