// https://www.sqlitetutorial.net/sqlite-nodejs/query/
// https://www.sqlitetutorial.net/sqlite-nodejs/statements-control-flow/

// For inspecting an objects members.
//const util = require('util');

let DbDebug = 0;

var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('JukeBox.db', (err) => {
	if(err) {
		console.log(err.message);
	}});

let fs = require('fs');
// sqlite3.OPEN_READONLY;


function InitalizeDB() {
	fs.readFile("JukeBox_DB.sql", 'utf8', function (error, pgResp) {
		if (error) {
			console.log('Cannot read from JukeBox_DB.sql');
		} else {
			console.log("Initalizing DB");
			db.serialize(() => {
				// db.run only runs the first command and drops the rest.
				// https://www.reddit.com/r/node/comments/3fxjkm/how_does_one_execute_a_sql_file_to_create_an/
				db.exec(pgResp, (err) => {
					if(err) {
						console.log(err.message);
					}
				});
			});
			// console.log(pgResp);			
			console.log("DB Initalized");
		}
	}); 
}

function Connect() {
	console.log("Connect");
	db.get("SELECT COUNT(name) COUNT FROM sqlite_master WHERE type='table' AND name='config'", (err,row) => {
			if(err) { 
				console.log("DB error is "+err.message);
			}
			if(DbDebug) console.log("Query row is "+row.COUNT);
			if(row.COUNT === 0) {
				InitalizeDB();
			}
		}
	);
}

function GetValue(Sql, CallBack) {	
	console.log("GetValue '" + Sql + "'");	
	db.get(Sql, function (err, row) {
		let ReturnData;		
		if(err) { console.log("SQL error in db.js "+err.message); }
		//if(DbDebug) console.log(util.inspect(row.value, {showHidden: false, depth: null}));
		for (var i in row) {
			ReturnData = row[i];
		}
		CallBack(ReturnData);
	});
}

function GetValues(Sql, CallBack) {
	let ReturnData = {};
	if(DbDebug) console.log("GetValues '" + Sql + "'");
	db.all(Sql, CallBack);
}

function Exec(Sql, CallBack) {
	if(DbDebug) console.log("Exec " + Sql);
	db.run(Sql, CallBack);	
}

module.exports={
	GetValue: GetValue,
	GetValues: GetValues,
	Exec: Exec
};

Connect();
