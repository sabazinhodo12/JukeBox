// Install NodeJS 
// curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.4/install.sh > NvmInstaller.sh
// bash ./NvmInstaller.sh
// . .bashrc
// nvm install 8

// For development this is nice to have
// npm install nodemon

// These are needed
// npm install express
// npm install sqlite3

// To Run 
// export NODE_PATH=$HOME/node_modules; nodemon /usr/local/DeployAPI/api/service.js


// https://www.twilio.com/blog/guide-node-js-logging
// https://stackabuse.com/how-to-start-a-node-server-examples-with-the-most-popular-frameworks/

// REST verbs
// https://restfulapi.net/http-methods/

let express = require('express');
let bodyParser = require('body-parser');
let app = express();
let favicon = require('serve-favicon');
let Db = require('./db');
let fs = require('fs');
let logger = require('./log');
const AdmZip = require('adm-zip');
const tempWrite = require('temp-write');
const path = require('path');
const NodeID3 = require('node-id3');


// Domain error handleing for NodeJS 10, maybe
// I tryed to wrap the ExpressJS in a Domain but it does nothing.
/*const Domain = require('domain').create();
Domain.on('error', (err) =>{
	console.log('Something broke '+err.message);
});
Domain.run( () => {
   require('http').createServer((req, res) => {
    handleRequest(req, res);
  }).listen(PORT);
});*/


logger.SetLog(app);

// https://codeforgeek.com/handle-get-post-request-express-4/
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(bodyParser.json()); // support json encoded bodies

// app.use(favicon(__dirname + '/public/images/favicon.ico'));
app.use(favicon('resource/img/favicon.ico'));


// For testing audio files.
app.use('/samples', express.static('samples'));

let ServePort = 6001;
if(typeof(process.env.PORT) !== "undefined") {
	ServePort = process.env.PORT;
}
console.log('The value of PORT is:', ServePort);


/************************************************************************************
Main: No API request.
************************************************************************************/
app.get('/',function(req,res) {
	fs.readFile("resource/main/main.html", function (error, pgResp) {
		
		// This dumps a lot of information about the request to the console.
		// console.log(req);

		if (error) {
			res.writeHead(404);
			res.write('Contents you are looking are Not Found');
		} else {
			res.writeHead(200, { 'Content-Type': 'text/html' });
			res.write(pgResp);
		}
		res.end();
	}); 
});

/************************************************************************************
resources like images, css or JS files.
************************************************************************************/
app.get('/resource/:res/:file',function(req,res) {
	fs.readFile("resource/"+req.params.res+"/"+req.params.file, function (error, pgResp) {
		if (error) {
			res.writeHead(404);
			res.write('Contents you are looking are Not Found');
		} else {
			res.writeHead(200, { 'Content-Type': 'text/html' });
			res.write(pgResp);
		}
		res.end();
	}); 
});

/************************************************************************************
TracksTable
************************************************************************************/
app.get('/TracksTable/:page?',function(req,res) {
	let WhereClause = " WHERE id > 0;";
	const Page = parseInt(req.params.page);

	if(typeof(Page) === "number") {		
		WhereClause = " WHERE id > 0 LIMIT 1000;";
		if(Page > 1) {
			WhereClause = " WHERE id > " + Page + " LIMIT 1000;";
		}
	}
	let Sql = "SELECT \
					id, \
					artist, \
					title, \
					year, \
					album, \
					genre \
				FROM tracks " + WhereClause;
	Sql = Sql.replace(/[\s]+/gi, " ");
	console.log(Sql);
	Db.GetValues(Sql, (err,row) => {
		res.send(JSON.stringify(row));
	});
});

/************************************************************************************
playlist
************************************************************************************/
app.get('/playlist/:playlistid',function(req,res) {
	const PlaylistId = req.params.playlistid;	
	let Sql = "SELECT \
					t.id, \
					t.artist, \
					t.title, \
					t.year, \
					t.album, \
					t.genre \
				FROM tracks t, labels l\
				WHERE l.labelName="+PlaylistId+" \
				AND t.id = l.track";
				//LIMIT 1000";

	Db.GetValues(Sql.replace(/[\s]+/gi, " "), (err,row) => {
		res.send(JSON.stringify(row));
	});	
});

/************************************************************************************
playlists
************************************************************************************/
app.get('/playlists',function(req,res) {
	let Sql = "SELECT id, labelName FROM labelNames WHERE labelName NOT NULL";
	Db.GetValues(Sql.replace(/[\s]+/gi, " "), (err,row) => {
		res.send(JSON.stringify(row));
	});
});

/************************************************************************************
newplaylist
************************************************************************************/
app.post('/newplaylist',function(req,res) {
	const Playlist = req.body.newplaylistname;
	let Sql = "SELECT COUNT(labelName) AS COUNT FROM labelNames WHERE labelName='"+Playlist+"'";
	Db.GetValues(Sql.replace(/[\s]+/gi, " "), (err,row) => {
		if(row[0].COUNT === 0) {
			Db.Exec("INSERT INTO labelNames (id, labelName) VALUES ((SELECT MAX(id)+1 FROM labelNames),'"+Playlist+"')");
			res.send('New playlist "'+Playlist+'" created.');
		} else {
			res.send('A list by that name aready exists. '+row[0].COUNT);
		}

	});
});


/************************************************************************************
addtoplaylist
************************************************************************************/
app.post('/addtoplaylist',function(req,res) {
	const PlaylistId = req.body.playlistid;
	const TrackId = req.body.trackid;
	let Sql = "SELECT COUNT(id) AS COUNT FROM labels WHERE labelName="+PlaylistId+" AND track="+TrackId;
	Db.GetValues(Sql.replace(/[\s]+/gi, " "), (err,row) => {
		if(row[0].COUNT === 0) {
			Db.Exec("INSERT INTO labels (id, labelName, track) VALUES ((SELECT MAX(id)+1 FROM labels),"+PlaylistId+", "+TrackId+")");
			res.send('Track added to playlist.');
		} else {
			res.send('That track is already in the playlist.');
		}
	});
});

/************************************************************************************
errorplayingtrack
************************************************************************************/
app.post('/errorplayingtrack',function(req,res) {
	console.log('Error playing track '+req.body.trackid);
	res.send('Track error reported.');
});


/************************************************************************************
removefromplaylist
************************************************************************************/
app.post('/removefromplaylist',function(req,res) {
	const PlaylistId = req.body.playlistid;
	const TrackId = req.body.trackid;
	let Sql = "SELECT COUNT(id) AS COUNT FROM labels WHERE labelName="+PlaylistId+" AND track="+TrackId;
	Db.GetValues(Sql.replace(/[\s]+/gi, " "), (err,row) => {
		if(row[0].COUNT > 0) {
			Db.Exec("DELETE FROM labels WHERE labelName="+PlaylistId+" AND track="+TrackId);
			res.send('Track removed from playlist.');
		} else {
			res.send('That track is not in the playlist.');
		}
	});
});

/************************************************************************************
playlists
************************************************************************************/
app.get('/download/:playlistid', function(req,res) {
	// Get all tracks in the playlist
	// Build an M3U file
	// Zip up the M3U and the tracks
	// respond with the binary zip file

	//let Sql = "SELECT id, labelName FROM labelNames WHERE labelName NOT NULL";
	//Db.GetValues(Sql.replace(/[\s]+/gi, " "), (err,row) => {
	//	res.send(JSON.stringify(row));
	//});

	// This is just an idea to manually create the file.
	//res.writeHead(200, {
	//	'Content-Type': 'application/zip',
	//	'Content-disposition': 'attachment;filename=playlist.zip',
    //    'Content-Length': data.length
    //});
    //res.end(Buffer.from(data, 'binary'));
	const PlaylistId = req.params.playlistid;	
	Db.GetValue("SELECT labelName FROM labelNames WHERE id = "+PlaylistId, (PlaylistName) => {
		Db.GetValue("SELECT value FROM config WHERE parameter = 'MusicPath'", (MusicRoot) => {
			let Sql = "SELECT \
						( p.path || '/' || t.track ) AS PathFile \
						FROM tracks t, labels l, paths p\
						WHERE l.labelName="+PlaylistId+" \
						AND t.id = l.track \
						AND t.path = p.id";
						//LIMIT 1000";
			
			Db.GetValues(Sql.replace(/[\s]+/gi, " "), (err,row) => {
				const zip = new AdmZip();
				const TempFile = tempWrite.sync('', PlaylistName+'.m3u');
				console.log('TempFile ='+TempFile);
				for (let i = 0; i < row.length; i++) {
					// addLocalFile(String localPath, String zipPath) 
					zip.addLocalFile(MusicRoot+'/' + row[i].PathFile, row[i].PathFile);
					console.log("append to playlist "+row[i].PathFile);
					fs.appendFile(TempFile, row[i].PathFile+'\r\n', (err) => {
						console.log('apending callback');
						if(err) throw err;
						if(i == row.length - 1) {
							console.log('addLocalFile TempFile ='+TempFile);
							zip.addLocalFile(TempFile);
							const data = zip.toBuffer();
							res.set('Content-Type','application/octet-stream');
							res.set('Content-Disposition','attachment; filename=Playlist.zip');
							res.set('Content-Length', data.length);
							console.log('sending zip file');
							res.send(data);						
						}
					});
				};
			});	
		});
	});
	// This works just fine to send one file. The zip will need many files. 
	//res.sendFile('/tmp/test.zip');	 
});

/************************************************************************************
play
************************************************************************************/
app.get('/play/:trackid',function(req,res) {
	let TrackId = req.params.trackid.replace(/.mp3$/, "");
	let Sql = "SELECT \
				( c.value || '/' || p.path || '/' || t.track ) AS PathFile \
				FROM tracks t, paths p, config c \
				WHERE t.id = "+TrackId+" \
				AND t.path = p.id \
				AND c.parameter = 'MusicPath' \
				LIMIT 1";

	Db.GetValue(Sql.replace(/[\s]+/gi, " "), (PathFile) => {
		console.log("res.sendFile('"+PathFile+"'');")

		// This is doing something to the file that Chrome does not like.
		// It gives an error about the MIME type not being audio.
		// When the MP3 file is served by express.static it works in Chrome.
		// res.sendFile(PathFile);

		let stat = fs.statSync(PathFile);
		// This is just an idea to manually create the file.
		//res.writeHead(200, {
		res.header({
			'Content-Type': 'audio/mp3',
	        'Content-Length': stat.size
	        //'Accept-Ranges': 'bytes'
    	});
    	//res.end(Buffer.from(data, 'binary'));


    	// This is cool but it also does not give the MP3 to Chrome correctly.
    	const rstream = fs.createReadStream(PathFile);
        rstream.pipe(res);
        

        // Nope, not that.
        //res.download(PathFile);
	});
});

/************************************************************************************
stream
************************************************************************************/
app.get('/stream/:trackid', (req, res) => {
	let TrackId = req.params.trackid.replace(/.mp3$/, "");
	let Sql = "SELECT \
				( c.value || '/' || p.path || '/' || t.track ) AS PathFile \
				FROM tracks t, paths p, config c \
				WHERE t.id = "+TrackId+" \
				AND t.path = p.id \
				AND c.parameter = 'MusicPath' \
				LIMIT 1";

	Db.GetValue(Sql.replace(/[\s]+/gi, " "), (file) => {
		const stat = fs.statSync(file);
		const total = stat.size;
		fs.exists(file, (exists) => {
			if (exists) {
				const range = req.headers.range;
				const parts = range.replace(/bytes=/, '').split('-');
				const partialStart = parts[0];
				const partialEnd = parts[1];

				const start = parseInt(partialStart, 10);
				const end = partialEnd ? parseInt(partialEnd, 10) : total - 1;
				const chunksize = (end - start) + 1;
				const rstream = fs.createReadStream(file, {start: start, end: end});

				res.writeHead(206, {
					'Content-Range': 'bytes ' + start + '-' + end + '/' + total,
					'Accept-Ranges': 'bytes', 'Content-Length': chunksize,
					'Content-Type': 'audio/mpeg'
				});
				rstream.pipe(res);

			} else {
				res.send('Error - 404');
				res.end();
				// res.writeHead(200, { 'Content-Length': total, 'Content-Type': 'audio/mpeg' });
				// fs.createReadStream(path).pipe(res);
			}
		});
	});
});

/************************************************************************************
albumart
************************************************************************************/
//app.get('/albumart/:trackid', async (req, res, next) => {
app.get('/albumart/:trackid', (req, res) => {
	let TrackId = req.params.trackid.replace(/.mp3$/, "");
	let Sql = "SELECT \
				( c.value || '/' || p.path || '/' || t.track ) AS PathFile \
				FROM tracks t, paths p, config c \
				WHERE t.id = "+TrackId+" \
				AND t.path = p.id \
				AND c.parameter = 'MusicPath' \
				LIMIT 1";

	Db.GetValue(Sql.replace(/[\s]+/gi, " "), (file) => {
		const stat = fs.statSync(file);
		const total = stat.size;
		fs.exists(file, (exists) => {
			if (exists) {

				let Dir = path.dirname(file);
				console.log('Dir = '+Dir);
				// Look for cover.jpg, folder.jpg, cover.png, or folder.png
				fs.readdir(Dir, (err, files) => {
					/*files.forEach(file => {
						console.log("files include "+file);
					});*/
				
					// Tested on 
					// Source:https://playcode.io/479116
					// Public:https://479116.playcode.io
					
					let ArtFiles = files.filter(function(value, index, arr){
						// This is wrong because it returns anything ending in jpg or png and disregards the rest of the filter.
  						// value.match(/(cover|front|folder)*(jpg|png)/i);
						return value.match(/(cover|front|folder)/i) && value.match(/(jpg|png)/i);
					});
					fs.exists(Dir+'/'+ArtFiles[0], (exists) => {
						if(exists) {
							res.sendFile(Dir+'/'+ArtFiles[0]);
							console.log("Art line is ", Dir+'/'+ArtFiles[0]);
						} else {
							console.log("Read ID3Tag of "+file);
							// This throws an error, let's catch it and handle it.
							
							// try throw catch error does not work in asynchronous mode.
							//try {

							// As of NodeJS V 8, node is asynchronous and needed to use domains to manage errors.
							// https://stackoverflow.com/questions/7310521/node-js-best-practice-exception-handling

							// As of NodeJS V 13, domains are deprecated but does not offer a replacement.
							// https://nodejs.org/api/domain.html
							NodeID3.read(file, function(err, tags) {								
								if (typeof tags.image.imageBuffer !== 'undefined') {
									if (typeof tags.image.imageBuffer.length !== 'undefined') {
										console.log('got a buffer');
										res.send(tags.image.imageBuffer);
									}								
								}
							});
							//} catch(err) {
							//	console.log("Error reading ID3Tag: "+err.message);
							//}
							/*
							tags: {
								title: "Tomorrow",
								artist: "Kevin Penkin",
								image: {
									mime: "jpeg",
									type: {
										id: 3,
										name: "front cover"
									},
									description: String,
									imageBuffer: Buffer
								},
								raw: {
									TIT2: "Tomorrow",
									TPE1: "Kevin Penkin",
									APIC: Object (See above)
								}
							}
							*/
						}
					});
				});
			} else {
				res.send('Error - 404');
				res.end();
			}
		});
	});
});

/************************************************************************************
TrackInfo
************************************************************************************/
app.get('/trackinfo/:trackid',function(req,res) {
	const TrackId = req.params.trackid;

	let Sql = "SELECT *	FROM tracks WHERE id = "+TrackId;

	Db.GetValues(Sql.replace(/[\s]+/gi, " "), (err,row) => {
		res.send(JSON.stringify(row));
	});
});


// Does express have it's own domain for error handleing?
// It looks like the answer is no. This does nothing.
/*app.on('error', (err) => {
	if(err) {
		console.log('Something broke '+err.message);
	} else {
		console.log('Something broke, but we don\'t have a message.');
	}
});*/


// ExpressJS should be able to deal with this.
// https://derickbailey.com/2014/09/06/proper-error-handling-in-expressjs-route-handlers/
// development error handler
// will print stacktrace
/*if (app.get('env') === 'development') {

  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: err
    });
  });

}*/

// production error handler
// no stacktraces leaked to user
/*app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});*/

var server = app.listen(ServePort, "0.0.0.0", function() {});



