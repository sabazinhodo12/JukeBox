

// https://medium.com/@tobydigz/logging-in-a-node-express-app-with-morgan-and-bunyan-30d9bf2c07a
//var log4js = require("log4js");
var morgan = require("morgan");

function SetLog(app) {
	//var theAppLog = log4js.getLogger();
	app.use(morgan({
		"format": "default",
		"stream": {
		write: function(str) { 
			//theAppLog.debug(str);
			console.log(str); 
		}
	}}));
}

module.exports={
	SetLog: SetLog
};



// This dumps a lot of information about the request to the console.
// console.log(req);


// https://stackoverflow.com/questions/19835652/whats-the-best-practice-for-expressjs-logging
//addRequestId = require('express-request-id')(); //(setHeader: false);
//app.use(addRequestId);

//morgan = require('morgan');

// morgan.token('id', (req) -> req.id.split('-')[0]);

//app.use(morgan("[:date[iso] #:id] Started :method :url for :remote-addr",immediate: true));

//app.use(morgan("[:date[iso] #:id] Completed :status :res[content-length] in :response-time ms"));

// app.use('/api', router);
