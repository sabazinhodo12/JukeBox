'Find a track details'
```SQL
SELECT * 
FROM labels L, labelNames N, tracks T, paths P 
WHERE T.path = P.id 
	AND L.labelName = N.id 
	AND L.track = T.id 
	AND L.id = 645 
ORDER BY id DESC 
LIMIT 5;
```

