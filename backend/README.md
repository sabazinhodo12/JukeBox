# JukeBox API, the backend


[nodemon](https://alligator.io/workflow/nodemon/)

# Installing NodeJS tools
```
sudo su
bash
cd
npm install -g pm2
npm install -g nodemon
```

Once these are installed globally they can be ran directly as shown below. Restart your terminal to make sure the path is loaded correctly.

## Development
```
nodemon ./api/service.js
```

## Production
```
pm2 ./api/service.js
```
