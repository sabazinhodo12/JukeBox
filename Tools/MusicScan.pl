#!/usr/bin/perl -w

use strict;
use warnings;
use DBI;
use File::Spec;
use File::Find;
use File::Basename;
use Digest::MD5;
use MP3::Tag;
use MP3::Info;
use Cwd 'abs_path';

my $DbFile = $ARGV[0];
my $MusicPath = $ARGV[1];
my $Debug = 0;
$Debug = 1 if $ARGV[2] eq "-d";

# Interleaving mode is for when the DB has already been created and we are updating it will new entries. It will disconnect between updates 
# to allow other programs access to the DB. If the DB is empty, it will use building mode for faster table population speed.
my $ConnectMode = "interleaving";

my $dbh;

sub ScanFile {
	# Only process files.
	return unless -f $_;

	# Only process if name ends with .mp3
	return unless $_ =~ /.mp3$/;
	my $PathFile = $_;

	printf("%s\n", $PathFile) if $Debug == 1;
	# I am sure there is a Perl regex to do this. I am too tired.
	my $Path = substr(dirname(abs_path($PathFile)), length($MusicPath) + 1);
	if ($Debug == 1) {
		printf("PathFile = '%s'\n", $PathFile);
		printf("abs_path = '%s'\n", abs_path($PathFile));
		printf("dirname abs_path = '%s'\n", dirname(abs_path($PathFile)));
		printf("MusicPath = '%s'\n", $MusicPath);
		printf("Path = '%s'\n", $Path);
	}
	my $File = basename($PathFile);	
	my $sth;
	my @Data;
	my $Pid;
	my $Md5;
	my $FileSize;

	if($ConnectMode eq "interleaving") {
		$dbh = DBI->connect("dbi:SQLite:dbname=$DbFile", "", "", {RaiseError =>1}) or die $DBI::errstr;
	
		# See if we need to change to building mode
		$sth = $dbh->prepare("SELECT COUNT(id) FROM paths LIMIT 1");
		$sth->execute();
		@Data = $sth->fetchrow_array();
		# A blank DB will have one blank row.
		if($Data[0] < 2) {
			$ConnectMode = "building";
			printf("DB looks blank, switching to building mode.\n");
			$dbh->prepare("REPLACE INTO config (parameter, value) VALUES ('MusicPath', ?)")->execute($MusicPath) || die "$DBI::errstr";
		}
	}

	$sth = $dbh->prepare("SELECT id FROM paths WHERE path = ? LIMIT 1");
	$sth->execute($Path);
	if(length(DBI::errstr)) {
		printf("SQLite error: %s\nSQL: SELECT id FROM paths WHERE path = '%s' LIMIT 1\n". $DBI::errstr, $Path);
	}
	@Data = $sth->fetchrow_array();
	if( ! defined $Data[0]) {
		# printf("INSERT INTO paths (id, path) VALUES ((SELECT MAX(id)+1 FROM paths), '%s')\n", $Path);
		# sleep(1);
		$dbh->prepare("INSERT INTO paths (id, path) VALUES ((SELECT MAX(id)+1 FROM paths), ?)")->execute($Path) || die "$DBI::errstr";
		$sth = $dbh->prepare("SELECT id FROM paths WHERE path = ? LIMIT 1");
		$sth->execute($Path);
		@Data = $sth->fetchrow_array();
	}
	$Pid = $Data[0];

	if(! -f $MusicPath."/".$Path."/".$File) {
		die "No such file '".$MusicPath."/".$Path."/".$File."'";
	}

	# md5sum
	open (my $fh, '<', $PathFile);
	binmode($fh);
	$Md5 = Digest::MD5->new->addfile($fh)->hexdigest;
	close($fh);

	# file size
	$FileSize = (stat($PathFile))[7];
	
	# Get the MP3 tag info
	my $Mp3 = MP3::Tag->new($PathFile) or die "Error reading ID3 Tag on '".$MusicPath."/".$Path."/".$File."'";
	$Mp3->get_tags;
	my $Id3Hash = $Mp3->autoinfo();
	my $Title = ''.$Id3Hash->{"title"};
	$Title = $Id3Hash->{"song"} if $Title eq '';

	my $TagHash = get_mp3info($PathFile);
	#foreach my $Tag (keys %{$Id3Hash}) {
	#	printf("TagHash->{'$Tag'} = '".$TagHash->{$Tag}."'\n")
	#}

	# Save the data
	$sth = $dbh->prepare("SELECT COUNT(id) FROM tracks WHERE path=? AND track=? LIMIT 1");
	$sth->execute($Path, $File);
	@Data = $sth->fetchrow_array();	
	if($Data[0] eq "0") {	
		my %DataPack = (			
			path => $Pid,
			track => $File,
			md5sum => $Md5,
			fileSizeBytes => $FileSize,
			artist => $Id3Hash->{"artist"},
			year => $Id3Hash->{"year"},
			album => $Id3Hash->{"album"},
			title => $Title,
			trackId => $Id3Hash->{"track"},
			genre => $Id3Hash->{"genre"},
			bitRate => $TagHash->{"BITRATE"},
			playTime => $TagHash->{"TIME"}
		);

		my @DataArray;
		my $Sql1 = "INSERT INTO tracks (id, ";
		my $Sql2 .= "VALUES ((SELECT MAX(id)+1 FROM tracks), ";
		foreach my $Key (keys %DataPack) {
			$Sql1 .= $Key.", ";
			$Sql2 .= "?, ";
			push(@DataArray, $DataPack{$Key});
		}
		$Sql1 =~ s/, $/)/;
		$Sql2 =~ s/, $/)/;
		$dbh->prepare($Sql1.$Sql2)->execute(@DataArray);
	}
	if($ConnectMode eq "interleaving") {
		$sth->finish();
		$dbh->disconnect();		
	}	


}

# Find all MP3 files in path.
find({wanted => \&ScanFile, no_chdir => 1}, $MusicPath);

